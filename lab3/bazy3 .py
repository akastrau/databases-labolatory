# -*- coding: utf-8 -*-
"""
Spyder Editor

Bazy - lab 3
"""
import pymysql as sql
import getpass
import argparse
from appJar import gui

def createUser():    # Interactive mode
    conn = sql.connect(host="localhost", user="root", password="", db ="test")
    username = input("Enter the username: ")
    passw = getpass.getpass("Enter password: ")
    query = "INSERT INTO USERS (login, pass) VALUES(%s, SHA2(%s, 256))"
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (username, passw))
            conn.commit()
        with conn.cursor() as cursor:
            cursor.execute("SELECT * FROM USERS")
            for result in cursor:
                print(result, end="")
    except Exception as ex:
        print(ex)
    finally:
        conn.close()

def specialMode():
    try:
        conn = sql.connect(host="localhost", user="root", password="", db ="test")
    except Exception as ex:
        print(ex)
    print("\nCommands: [show users asc, show users desc]")
    command = input("Enter the command: ")
    if command == "show users asc":
        try:
            with conn.cursor(sql.cursors.DictCursor) as cursor:
                cursor.execute("SELECT * FROM USERS ORDER BY login ASC")
                result = cursor.fetchall()
                print("id\t\tlogin\n======================================")
                for user in result:
                    print(str(user["id"]) + "\t\t" + user["login"])
        except Exception as ex:
            print(ex)
    elif command == "show users desc":
        try:
            with conn.cursor(sql.cursors.DictCursor) as cursor:
                cursor.execute("SELECT * FROM USERS ORDER BY login DESC")
                result = cursor.fetchall()
                print("id\t\tlogin\n======================================")
                for user in result:
                    print(str(user["id"]) + "\t\t" + user["login"])
        except Exception as ex:
            print(ex)
    conn.close()

def login():
    print("Bazy lab3 interactive login console\n===============================")
    try: 
        conn = sql.connect(host="localhost", user="root", password="", db ="test")
    except Exception as ex:
        print(ex)
    login = input("Enter the username: ")
    passw = getpass.getpass("Enter password: ")
    try:
        with conn.cursor() as cursor:
            query = "SELECT * FROM USERS WHERE login=%s AND pass=SHA2(%s, 256)"
            cursor.execute(query, (login, passw))
            result = cursor.fetchone()
            if result is None:
                print("Incorrect login or password!")
                input("Press ENTER to exit...")
            else:
                print("\nHello " + login + "!")
                print("User's ID: ", end="")
                print(str(result[0]) + "\n")
                if login == "admin":
                    specialMode()
                    print()
                input("Press ENTER to exit...")
    except Exception as ex:
        print(ex)
    conn.close()

def guiLogin():

    def validateCredentials(button):
        if button == "Cancel":
            ui.stop()
        else:
            ui.disableButton()

    ui = gui("Login")
    ui.setFont(14)
    ui.setResizable(False)

    ui.addLabel("title", "Interactive login console", 0, 0, 2)

    ui.addLabel("user", "User name:", 1, 0)
    ui.addEntry("user", 1, 1)

    ui.addLabel("pass", "Password:", 2, 0)
    ui.addSecretEntry("pass", 2, 1)

    ui.addButtons(["Log in", "Cancel"], validateCredentials, 3, 0, 2)

    ui.setFocus("user")

    ui.go()


def main():
    parser = argparse.ArgumentParser(description="Script to log in to the database")
    parser.add_argument("--createUser", "-c", help="creates a user", action="store_true")
    parser.add_argument("--login", "-l", help="interactive login shell", action="store_true")
    args = parser.parse_args()
    if args.login:
        login()
    elif args.createUser:
        createUser()
    else:
        guiLogin()

main()
