# -*- coding: utf-8 -*-
"""
Spyder Editor

Bazy - lab 2
"""
import pymysql as sql
conn = sql.connect(host="localhost", user="root", password="", db ="test")
text = input("Podaj tekst: ")
query = "INSERT INTO tab VALUES(%s)"
try:
    with conn.cursor() as cursor:
        cursor.execute(query, (text))
        conn.commit()
    with conn.cursor() as cursor:
        cursor.execute("SELECT * FROM tab")
        for result in cursor:
            print(result, end="")
except Exception as ex:
    print(ex)
finally:
    conn.close()