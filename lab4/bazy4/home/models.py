# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Faculty(models.Model):
    idfaculty = models.AutoField(primary_key=True)
    facultyname = models.CharField('faculty name', db_column='facultyName', unique=True, max_length=60)  # Field name made lowercase.
    faculty = models.Manager()

    def __str__(self):
        return self.facultyname

    class Meta:
        db_table = 'faculty'
        verbose_name_plural = 'faculties'


class Staff(models.Model):
    idstaff = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=80)
    lastname = models.CharField(max_length=90)
    email = models.CharField(unique=True, max_length=60)
    password = models.CharField(max_length=64)
    faculty_idfaculty = models.ForeignKey(Faculty, models.DO_NOTHING, db_column='faculty_idfaculty')

    def __str__(self):
        return self.firstname + ' ' + self.lastname

    class Meta:
        db_table = 'staff'


class Student(models.Model):
    idstudent = models.AutoField(primary_key=True)
    fristname = models.CharField(max_length=80)
    lastname = models.CharField(max_length=100)
    email = models.CharField(max_length=60)
    password = models.CharField(max_length=64)
    addressline = models.CharField(db_column='addressLine', max_length=100)  # Field name made lowercase.
    city = models.CharField(max_length=45)
    zipcode = models.CharField(db_column='zipCode', max_length=5)  # Field name made lowercase.
    faculty_idfaculty = models.ForeignKey(Faculty, models.DO_NOTHING, db_column='faculty_idfaculty')

    student = models.Manager()

    def __str__(self):
        return self.fristname + ' ' + self.lastname

    class Meta:
        db_table = 'student'