from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import *


def new_faculty(request):
    faculty = ''
    if request.method == 'POST':
        form = AddFacultyForm(request.POST)
        faculty = request.POST.get('facultyname')
        if form.is_valid():
            faculty = form.cleaned_data['facultyname']
            form.save()
            return render(request, 'add-faculty-success.html', {'name': faculty})
    else:
        form = AddFacultyForm()

    return render(request, 'add-faculty.html', {'form': form, 'faculty': faculty})


def show_faculties(request):
    from .models import Faculty
    param = request.GET.get('sort')
    if request.GET.get('sort') == 'asc':
        faculties = Faculty.faculty.order_by('facultyname')
    elif request.GET.get('sort') == 'desc':
        faculties = Faculty.faculty.order_by('-facultyname')
    else:
        faculties = Faculty.faculty.order_by('facultyname')

    return render(request, 'view-faculties.html', {'faculties': faculties, 'sort': param})


def add_student(request):
    faculties = Faculty.faculty.order_by('facultyname')
    if request.method == 'POST':
        form = AddStudentForm(request.POST, edit=False)
        if form.is_valid():
            name = form.cleaned_data.get(
                'fristname') + ' ' + form.cleaned_data.get('lastname')
            form.save()
            student = Student.student.get(email=form.cleaned_data.get('email'))
            return render(request, 'add-student-success.html', {'name': name, 'id': student.pk})
        else:
            requestDict = request.POST.dict()
            print()
            return render(request, 'add-student.html', {'form': form, 'options': faculties, 'data': requestDict, 'id' : requestDict.get('faculty_idfaculty'), 'edit' : False})
    else:
        form = AddStudentForm()

    return render(request, 'add-student.html', {'form': form, 'options': faculties})


def show_students(request):
    from .models import Faculty, Student
    sortFirstName = request.GET.get('sortFirstName')
    sortLastName = request.GET.get('sortLastName')
    sortEmail = request.GET.get('sortEmail')
    sortFaculty = request.GET.get('sortFaculty')

    if sortFirstName == 'asc':
        students = Student.student.order_by('fristname')
    elif sortFirstName == 'desc':
        students = Student.student.order_by('-fristname')
    elif sortLastName == 'asc':
        students = Student.student.order_by('lastname')
    elif sortLastName == 'desc':
        students = Student.student.order_by('-lastname')
    elif sortEmail == 'asc':
        students = Student.student.order_by('email')
    elif sortEmail == 'desc':
        students = Student.student.order_by('-email')
    elif sortFaculty == 'asc':
        students = Student.student.order_by('faculty_idfaculty')
    elif sortFaculty == 'desc':
        students = Student.student.order_by('-faculty_idfaculty')
    else:
        students = Student.student.order_by('fristname')
    facultyID = request.GET.get('from_faculty')
    if facultyID is not None:
        try:
            if Student.student.filter(faculty_idfaculty=facultyID).count() == 0:
                return render(request, 'view-students-faculty.html', {'result' : False})
            students = Student.student.filter(faculty_idfaculty=facultyID)
        except Exception:
            return render(request, 'view-students-faculty.html', {'result' : False})
        return render(request, 'view-students-faculty.html', {'result' : True, 'students' : students})
    id = request.GET.get('id')
    if id is not None:
        try:
            student = Student.student.get(pk=id)
        except Exception:
            return render(request, 'error.html', {'error': 'Student not found!'})
        return render(request, 'show-student-info.html', {'student': student})

    return render(request, 'view-students.html', {'students': students, 'sortFirstName': sortFirstName,
                                                  'sortLastName': sortLastName, 'sortEmail': sortEmail, 'sortFaculty': sortFaculty})


def edit_student(request):
    from django.forms.models import model_to_dict
    id = request.GET.get('id')
    if id is not None:
        try:
            student = Student.student.get(pk=id)
        except Exception:
            return render(request, 'error.html', {'error': 'Student not found!'})
    else:
        return render(request, 'error.html', {'error': 'Wrong argument!'})
    faculties = Faculty.faculty.order_by('facultyname')
    if request.method == 'POST':
        form = AddStudentForm(request.POST, edit=True, id=id, instance=student)
        if form.is_valid():
            form.save()
            return render(request, 'edit-student-success.html', {'name': student.fristname + ' ' + student.lastname,
                                                                 'id': student.pk})
    else:
        form = AddStudentForm()
    return render(request, 'add-student.html', {'form': form, 'data':  model_to_dict(student), 'options': faculties,
                                                'id': student.pk, 'edit': True})

def delete_student(request):
    if request.method == 'POST':
        studentid = request.POST.get('id')
        try:
            student = Student.student.get(pk=studentid)
        except Exception:
            return render(request, 'error.html', {'error': 'Student not found!'})
        try:
            student.delete()
        except Exception:
            return render(request, 'error.html', {'error': 'This student cannot be deleted ;)'})
        return render(request, 'delete-student.html')
    return render(request, 'error.html', {'error': 'Wrong form action!'})

def search_student(request):
    firstname = request.GET.get('searchFirstName')
    lastname = request.GET.get('searchLastName')
    if len(request.GET) < 3:
           return render(request, 'search-students.html')
    if firstname is '' and lastname is '':
        return render(request, 'search-students.html')
    elif firstname is '':
        try:
            student = Student.student.filter(lastname__icontains=lastname)
            if Student.student.filter(lastname__icontains=lastname).count() == 0:
                raise 'No students!'
        except Exception:
            student = None
    elif lastname is '':
        try:
            student = Student.student.filter(fristname__icontains=firstname)
            if Student.student.filter(fristname__icontains=firstname).count() == 0:
                raise 'No students!'
        except Exception:
            student = None
    else:
        try:
            student = Student.student.filter(fristname__icontains=firstname, lastname__icontains=lastname)
            if Student.student.filter(fristname__icontains=firstname, lastname__icontains=lastname).count() == 0:
                raise 'No students!'
        except Exception:
            student = None
    if student is not None:
        return render(request, 'search-students.html', {'students' : student, 'result' : True})
    else:
        print('Err')
        return render(request, 'search-students.html', {'result' : False})

def index(request):
    return render(request, 'main.html')


def contact(request):
    return render(request, 'contact.html')


def about(request):
    return render(request, 'about.html')
