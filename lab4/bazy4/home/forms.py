from django import forms
from home.models import *


class StudentForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Student
        fields = '__all__'


class StaffForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Staff
        fields = '__all__'


class AddFacultyForm(forms.ModelForm):
    facultyname = forms.CharField(label='Faculty name', max_length=60)

    class Meta:
        model = Faculty
        fields = '__all__'


class AddStudentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.edit = kwargs.pop('edit', None)
        self.id = kwargs.pop('id', None)
        super(AddStudentForm, self).__init__(*args, **kwargs)
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Student
        fields = '__all__'

    def clean(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        if self.edit is False:
            if Student.student.filter(email=self.cleaned_data.get('email')).exists():
                raise forms.ValidationError(
                    "User with that email already exists!")
        else:
            email = Student.student.get(
                pk=self.id).email
            if Student.student.filter(email=self.cleaned_data.get('email')).exists():
                if self.cleaned_data.get('email').strip() != email.strip():
                    raise forms.ValidationError(
                        "User with that email already exists!")

        return self.cleaned_data
