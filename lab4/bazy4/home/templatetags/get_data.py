from django import template

register = template.Library()

@register.simple_tag
def get_data(key, objectPost):
    try:
        data = objectPost.get(key)
    except Exception:
        print(key)
        print(objectPost)
        return ''
    return data
