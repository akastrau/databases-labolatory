"""bazy4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^about/$', views.about, name='about'),
    url(r'^add-faculty/$', views.new_faculty, name='new faculty'),
    url(r'^view-faculties/$', views.show_faculties, name='Faculty list'),
    url(r'^add-student/$', views.add_student, name='new student'),
    url(r'^view-students/$', views.show_students, name='Student list'),
    url(r'^edit-student/$', views.edit_student, name='new student'),
    url(r'^delete-student/$', views.delete_student, name='delete student'),
    url(r'^search-students/$', views.search_student, name='search student'),

]
