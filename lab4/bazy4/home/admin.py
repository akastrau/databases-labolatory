from django.contrib import admin
from home.forms import *

# Register your models here.
from home.models import *
class StudentAdmin(admin.ModelAdmin):
    form = StudentForm
class Staffdmin(admin.ModelAdmin):
    form = StaffForm

admin.site.register(Faculty)
admin.site.register(Student, StudentAdmin)
admin.site.register(Staff, Staffdmin)