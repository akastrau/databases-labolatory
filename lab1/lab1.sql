USE test;

CREATE TABLE test
(
id INT,
tekst VARCHAR(20)
);

INSERT INTO test VALUES(1, "ghfghfhh");

SELECT * FROM test;

INSERT INTO test VALUES(NULL, "ghfghfhh");
SELECT * FROM test;

SELECT(2+NULL);
SELECT(2*NULL);
SELECT(2+NULL) IS NULL;


CREATE TABLE test1
(
id INT NOT NULL,
tekst VARCHAR(20)
);

INSERT INTO test1 VALUES(NULL, "ghfghfhh");
INSERT INTO test1 VALUES(1, "ghfghfhh");
INSERT INTO test1(id, tekst) VALUES(2, "ghfghfhh");
INSERT INTO test1() VALUES();
desc test1;
SELECT * FROM test1;

CREATE TABLE test2
(
id INT NOT NULL PRIMARY KEY,
tekst VARCHAR(20)
);

INSERT INTO test2 VALUES(NULL, "ghfghfhh");
INSERT INTO test2 VALUES(1, "ghfghfhh");
INSERT INTO test2(id, tekst) VALUES(2, "ghfghfhh");
INSERT INTO test2() VALUES();

SELECT * FROM test2;

CREATE TABLE test3
(
id CHAR(36) NULL PRIMARY KEY,
tekst VARCHAR(20)
);
INSERT INTO test3 VALUES(UUID(), "ghhfghfggh");
SELECT * FROM test3;

CREATE TABLE test4
(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
tekst VARCHAR(20)
);

INSERT INTO test4() VALUES();

SELECT * FROM test4;

CREATE TABLE test5
(
id INT NOT NULL,
tekst VARCHAR(20),
PRIMARY KEY(id, tekst)
);

INSERT INTO test5 VALUES(2, "htfhfh tekst");


CREATE TABLE test6
(
id INT NOT NULL AUTO_INCREMENT,
tekst VARCHAR(20),
tekst2 VARCHAR(32),
d1 DECIMAL(5,2),
d2 DECIMAL(5,5),
z FLOAT,
PRIMARY KEY(id)
);

INSERT INTO test6(tekst, tekst2, d1, d2, z) VALUES("fhg", "1234567", 1.635464, 1.6345, 1/3);
INSERT INTO test6(tekst, tekst2, d1, d2, z) VALUES("Jan", "Kowalski", 1.635464, 1.6345, 3.5);
INSERT INTO test6(tekst, tekst2, d1, d2, z) VALUES("Stefan", "Kowalski", 1.635464, 1.6345, 4.5);
INSERT INTO test6(tekst, tekst2, d1, d2, z) VALUES("Marek", "Smutek", 1.635464, 1.6345, 5.0);

SELECT * FROM test6;

SELECT id, tekst, tekst2, d1, d2, z FROM test6 WHERE z>4;
SELECT id, tekst, tekst2, d1, d2, z FROM test6 WHERE z>4 AND tekst like "Stefan";


SELECT id, tekst, tekst2, d1, d2, z FROM test6 WHERE z>4 AND LENGTH(tekst) > 4;

SELECT id, tekst, tekst2, d1, d2, z FROM test6 ORDER BY tekst ASC, z DESC; 

SELECT id, tekst, tekst2, d1, d2, z FROM test6 ORDER BY rand() LIMIT 1;

SELECT tekst, COUNT(id) FROM test6 GROUP BY tekst;  

-- Lepiej bez null

SELECT DISTINCT tekst from test6;
-- Wycinanie rekordow powtarzajacych

SELECT tekst, COUNT(id), AVG(z) FROM test6
WHERE (z>=3) AND (z<=5)
GROUP BY tekst HAVING AVG(z) > 4;

SELECT tekst, COUNT(id) AS 'ile ocen', AVG(z) srednia FROM test6
WHERE (z>=3) AND (z<=5)
GROUP BY tekst HAVING AVG(z) > 4;

CREATE TABLE rodzic
(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nazwa_rodzic VARCHAR(20)
);

CREATE TABLE dziecko
(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_rodzic INT NOT NULL,
    nazwa_dziecko VARCHAR(20),
    CONSTRAINT fk_r_d FOREIGN KEY(id_rodzic)
		REFERENCES rodzic(id)
);

INSERT INTO rodzic VALUES(1, "ghfghfgh");
SELECT * FROM rodzic;

INSERT INTO dziecko VALUES(NULL, 1, "hfhfggh");
SELECT * FROM dziecko;

SELECT * from dziecko;

DELETE FROM rodzic WHERE id=1;
DELETE FROM dziecko WHERE id=1;